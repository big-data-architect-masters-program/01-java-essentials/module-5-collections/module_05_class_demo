package co.edureka.javaUtil;

import java.util.Calendar;
import java.util.Date;

public class JavaUtilDemo {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date);
        // calendar
        Calendar calendar = Calendar.getInstance();
        int hr = calendar.get(Calendar.HOUR_OF_DAY);
        int mn = calendar.get(Calendar.MINUTE);
        int s = calendar.get(Calendar.SECOND);
        System.out.println(hr+":"+mn+":"+s);
    }
}
