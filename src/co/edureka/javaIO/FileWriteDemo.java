package co.edureka.javaIO;

import java.io.*;

public class FileWriteDemo {
    public static void main(String[] args) throws IOException {
        File file = new File("/Users/sarathlun/IntelliJIDEAProjects/Module_05_Class_Demo/src/co/edureka/javaIO/file1.txt");
        FileInputStream fi = null;
        FileOutputStream fo = null;
        try {
            fi = new FileInputStream(file);
            fo = new FileOutputStream("/Users/sarathlun/IntelliJIDEAProjects/Module_05_Class_Demo/src/co/edureka/javaIO/file2.txt");
            int content;
            while ((content = fi.read()) != -1) {
                fo.write(content);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        fi.close();
        fo.close();
    }
}
