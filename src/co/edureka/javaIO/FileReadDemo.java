package co.edureka.javaIO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReadDemo {
    public static void main(String[] args) throws IOException {
        File file = new File("/Users/sarathlun/IntelliJIDEAProjects/Module_05_Class_Demo/src/co/edureka/javaIO/file1.txt");
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            System.out.println("Total Size:" + fileInputStream.available());
            int content;
            while ((content = fileInputStream.read()) != -1) {
                System.out.print((char) content);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
