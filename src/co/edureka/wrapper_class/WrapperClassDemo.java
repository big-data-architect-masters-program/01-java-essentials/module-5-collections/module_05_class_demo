package co.edureka.wrapper_class;

public class WrapperClassDemo {
    public static void main(String[] args) {
        // boxing: convert into reference
        Integer integer = Integer.valueOf(10);
        Character character = Character.valueOf('A');

        // un-boxing: take out the value
        System.out.println(integer.intValue());
        System.out.println(character.charValue());
    }
}
